Rails.application.routes.draw do

  get 'pages/contact_us'

  get 'pages/index'

  get 'pages/about_us'

  get 'pages/faq'

  get 'pages/tos'

  get 'login', to: 'pages#login'

  delete 'logout', to: 'pages#destroy'

  post 'login', to: 'pages#create'

  root 'pages#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
