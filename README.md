# README

Este README cuenta con la documentación necesaria para implementar el proyecto.

* Requerimientos

        Ruby version:   ruby 2.4.2p198 (2017-09-14 revision 59899) [x64-mingw32]
        Rails version:  Rails 5.1.4
        Gemas:          jquery-rails, bootstrap-sass v3.3.6, bcrypt v3.1.7

* Instrucciones para ejecutar

        1. Abrir consola SO en ruta de proyecto.
        2. Ejecutar comando: "bundle install"
        3. Ejecutar comando: "rails db:migrate"
        4. Ejecutar comando: "rake db:seed"
        5. Ejecutar comando: "rails c"
        6. Ejecutar comando: "u = User.first"
        7. Ejecutar comando: "u.password = 'password'"
        8. Ejecutar comando: "u.save"
        9. Ejecutar comando: "exit"
        10. Ejecutar comando: "rails s"
        11. Loguearse con credenciales: email='admin@gmail.com' & password='password'

* Observaciones

        1. En caso ocurra error al loguearse en el aplicativo y la descripción del mensaje sea la siguiente:
           'cannot load such file -- bcrypt'. seguir las siguientes instrucciones
                1.1. Ingresar al archivo Gemfile y eliminar la última fila: "gem 'bcrypt', '~> 3.1.11', platforms: [:ruby, :x64_mingw, :mingw]".
                1.2. Abrir consola SO en ruta de proyecto:
                1.3. Ejecutar comando: "gem uninstall bcrypt".
                1.3. Ejecutar comando: "gem install bcrypt --platform=ruby".
                1.4. Ingresar al archivo Gemfile y agregar: "gem 'bcrypt', '~> 3.1.11', platforms: [:ruby, :x64_mingw, :mingw]".
                1.5. Abrir consola SO en ruta de proyecto y ejecutar comando: "bundle install".
                1.6. Ejecutar comando: "rails s".