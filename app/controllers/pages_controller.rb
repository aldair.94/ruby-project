class PagesController < ApplicationController
  layout 'internal', except: [:index]

  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?

  def index
  end

  def about_us
  end

  def faq
  end

  def tos
  end
  
  def contact_us
  end

  def login
  end

  def create
    user = User.find_by(email: params[:email])

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id

      flash[:success] = "Bienvenido a Wok & Wine"

      redirect_to root_path
    else
      flash.now[:danger] = "Tu correo o contraseña no son correctas o no existen"
      render 'login'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "Hasta pronto..."
    redirect_to root_path
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end
end
